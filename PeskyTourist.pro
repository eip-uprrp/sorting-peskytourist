#-------------------------------------------------
#
# Project created by QtCreator 2014-03-23T19:32:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PeskyTourist
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Filter.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += \
    images.qrc
